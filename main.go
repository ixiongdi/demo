package main

import (
	"github.com/valyala/fasthttp"
	"fmt"
)

func fastHttpHandler(ctx *fasthttp.RequestCtx) {
	fmt.Fprint(ctx, "Hello, world!")
}

func main() {
	fasthttp.ListenAndServe(":8080", fastHttpHandler)
}
